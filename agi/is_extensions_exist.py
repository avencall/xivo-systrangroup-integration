#!/usr/bin/python
# -*- coding: UTF-8 -*-
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your OPTION) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
__version__   = '1.0'
__date__      = '20211013'
__author__    = 'Jean-Baptiste MARIN'

import requests
import json
from pprint import pprint
from xivo.agi import AGI

agi = AGI()

AUTH_TOKEN = ""
IP_XIVO="127.0.0.1"

extension_num = agi.get_variable("EXTENSION_NUM")
agi.set_variable("AGI_RETURN","False")

requests.packages.urllib3.disable_warnings()
session = requests.Session()
session.verify = False
session.headers = {'Accept': 'application/json', 'Content-Type': 'application/json'}

#CREATION DE L URL
XIVO_MAIN  = "https://"+IP_XIVO+":9486/1.1/extensions?search="


#EXECUTION DE LA REQUETE
request_url = "{}{}".format(XIVO_MAIN,extension_num)
response = session.get(request_url).json()

if response['total'] == 0:
    agi.verbose("### VERBOSE AGI  : L EXTENSION N A PAS ETE TROUVE PAR L AGI ###")
    agi.set_variable("AGI_RETURN","False")
else:
    agi.verbose("### VERBOSE AGI  : L EXTENSION EXISTE !!! ###")
    agi.set_variable("AGI_RETURN","True")

